<?php

namespace app\controllers;

use Yii;
use app\models\Provincias;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProvinciasController implements the CRUD actions for Provincias model.
 */
class ProvinciasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Provincias models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Provincias::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Provincias model.
     * @param string $autonomia
     * @param string $provincia
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($autonomia, $provincia)
    {
        return $this->render('view', [
            'model' => $this->findModel($autonomia, $provincia),
        ]);
    }

    /**
     * Creates a new Provincias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Provincias();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'autonomia' => $model->autonomia, 'provincia' => $model->provincia]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Provincias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $autonomia
     * @param string $provincia
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($autonomia, $provincia)
    {
        $model = $this->findModel($autonomia, $provincia);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'autonomia' => $model->autonomia, 'provincia' => $model->provincia]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Provincias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $autonomia
     * @param string $provincia
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($autonomia, $provincia)
    {
        $this->findModel($autonomia, $provincia)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Provincias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $autonomia
     * @param string $provincia
     * @return Provincias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($autonomia, $provincia)
    {
        if (($model = Provincias::findOne(['autonomia' => $autonomia, 'provincia' => $provincia])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
