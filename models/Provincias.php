<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provincias".
 *
 * @property string $autonomia
 * @property string $provincia
 * @property int $poblacion
 * @property int $superficie
 */
class Provincias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['poblacion', 'superficie'], 'integer'],
            [['autonomia', 'provincia'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'autonomia' => 'Autonomias',
            'provincia' => 'Provincias',
            'poblacion' => 'Poblacion',
            'superficie' => 'Superficie',
        ];
    }
}
